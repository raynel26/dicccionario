template <typename T>
string_map<T>::string_map(): raiz(nullptr), _size(0){}


template<class T>
void string_map<T>::insert(const pair<string, T> &c) {
    if (this->empty()){
        raiz = new Nodo(Nodo());
    }
    Nodo* p = raiz;
    int i = 0;
    int longitudClave = (c.first).size();

    while (i < longitudClave){
        if((p->siguientes)[int(c.first[i])] == nullptr){
            (p->siguientes)[int(c.first[i])] = new Nodo(Nodo());
            p->hijos++;
        }
        p = (p->siguientes)[int(c.first[i])];
        i++;
    }
    if(p->definicion == nullptr){
        p->definicion = new T(c.second);
    }else{
        *(p->definicion) = c.second;
    }
    _size++;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    if (d.empty()){
        raiz = nullptr;
    }else{
        raiz = new Nodo(Nodo());
        Nodo* p = raiz;
        Nodo* q = d.raiz;
        Copia_Recursiva(p, q);
    }
    return *this;
}

template<class T>
void string_map<T>::Copia_Recursiva(Nodo *&nodo, Nodo* &nodoDecopia) {
    if (nodoDecopia->definicion != nullptr){
        nodo->definicion = new T(*(nodoDecopia->definicion));
    }
    nodo->hijos = nodoDecopia->hijos;
    int i = 0;
    while (i < 256){
        if ((nodoDecopia->siguientes)[i] != nullptr){
            (nodo->siguientes)[i] = new Nodo(Nodo());
            Copia_Recursiva( (nodo->siguientes)[i], (nodoDecopia->siguientes)[i]);
        }
        i++;
    }
}

template <typename T>
string_map<T>::~string_map(){
    if(raiz != nullptr){
        destructorRecursivo(raiz);
    }
}

template<class T>
void string_map<T>::destructorRecursivo(Nodo *&nodo) {
    int i = 0;
    while (i < 256){
        if (nodo->siguientes[i] != nullptr){
            destructorRecursivo(nodo->siguientes[i]);
        }
        i++;
    }
    if (nodo->definicion != nullptr){
        delete nodo->definicion;
    }
    delete nodo;
}


template <typename T>
T& string_map<T>::operator[](const string& clave){
    //Completar
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    int res = 1;
    Nodo* p = raiz;
    if(this->empty()){
        return 0;
    }else{
        int i = 0;
        while (i < clave.size() && (p->siguientes)[int(clave[i])] != nullptr){
            p = (p->siguientes)[int(clave[i])];
            i++;
        }
        if (i < clave.size() || (i == clave.size() && p->definicion == nullptr)){
            res = 0;
        }
    }
    return res;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* p = raiz;
    int i = 0;
    int longitud_De_Clave = clave.size();
    while (i < longitud_De_Clave){
        char letra = clave[i];
        p = (p->siguientes)[int(letra)];
        i++;
    }
    return *(p->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* p = raiz;
    int i = 0;
    int longitud_De_Clave = clave.size();
    while (i < longitud_De_Clave){
        char letra = clave[i];
        p = (p->siguientes)[int(letra)];
        i++;
    }
    return *(p->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave){
    Nodo* ultimoNodoAsalvar = nullptr;
    if (raiz->hijos > 1){
        ultimoNodoAsalvar = raiz;
    }
    Nodo* p = raiz;
    int i = 0;
    int longitud_clave = clave.size();
    int k = 0;

    while (i < longitud_clave){ //ray
        p = (p->siguientes)[int(clave[i])];
        if (i == longitud_clave - 1 && p->hijos != 0){
            ultimoNodoAsalvar = p;
        }else{
            if(p->hijos > 1 || p->definicion != nullptr){
                ultimoNodoAsalvar = p;
                k = i + 1;
            }
        }
        i++;
    }
    _size--;
    delete p->definicion;
    p->definicion = nullptr;
    if (ultimoNodoAsalvar == nullptr){
        delete raiz;
        raiz = nullptr;
        eliminarpalabraRecursivo(raiz->siguientes[int(clave[0])], clave, 0);
    }else{
        if (ultimoNodoAsalvar != p){
            ultimoNodoAsalvar->hijos--;
            eliminarpalabraRecursivo(ultimoNodoAsalvar->siguientes[int(clave[k])], clave, k);
            ultimoNodoAsalvar->siguientes[int(clave[k])] = nullptr;
        }
    }
}

template<class T>
void string_map<T>::eliminarpalabraRecursivo(Nodo *&nodo, const string &clave, int k) {
    if(k < clave.size()){
        eliminarpalabraRecursivo(nodo->siguientes[int(clave[k + 1])], clave, k + 1);
        delete nodo;
    }
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
   return raiz == nullptr;
}